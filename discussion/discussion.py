# Python has several structures to store collections or multiple items in a single variable

# Lists
# Lists are similar to arrays in JavaScript in a sense that they can contain a collection of data
# To create a list, the square bracket ([]) is used.

names = ["John", "Paul", "George", "Ringo"]
programs = ['developer career', 'pi-shape', 'short courses']
durations = [260, 180, 20]
truth_values = [True, False, True, True, False]

# A list can contain elements of different data types
sample_list = ["Apple", 3, False, "Potato", 4, True]
print(sample_list)

# Getting the list size
# The number of elements in a list can be counted using the len() method
print(len(programs))

# Accessing values 
# List can be accessed by providing the index number of the element

# Access the first item in the list 
print(programs[0])

# Access the last item in the list
print(programs[-1])

# Access the second item in the list
print(durations[1])

# Access the whole list
print(durations)

# Access a range of values 
# list[start index: end index]
print(programs[0:2])

# Note that the end index is not included

# Updating lists
# Print the current value
print(f'Current value: {programs[2]}')
# Update the value 
programs[2] = 'Short Courses'

# Print the new value
print(f'Current value: {programs[2]}')

# Mini-exercise
# 1. Create a list of names of 5 students
# 2. Create a list of grades for the  5 students
# 3. Use a loop to iterate through the lists printing in the following format: 
# The grade of student1 is grades1

students = ["Vanessa", "Philippe", "Genevieve", "Marcus", "Ozaias"]
grades = [95, 85, 90, 89, 80]

j = 0;
while j < 5:
	print(f'The grade of {students[j]} is {grades[j]}')
	j+=1

# List manipulation
# List has methods that can be used to manipulate the elements within

# Adding List items - the append() method allows to insert items to a list

programs.append('global')
print(programs)

# Deleting List items - the "del" keyword can be used to delete elements in the list
# Add new item to the duration list 
durations.append(360)
print(durations)

# Delete the last item on the list
del durations[-1]
print(durations)

# Membership checks - the "in" keyword checks if the element is in the list
# Returns true or false
print(20 in durations)
print(500 in durations)

 # Sorting lists - the sort() method that sorts the list alphanumerically, ascending by default.
print(names)
names.sort()
print(names)

# Emptying the list - the clear() method is used to empty the contents of the list

test_list = [1, 3, 5, 7, 9]
print(test_list)
test_list.clear()
print(test_list)

# Dictionaries are used to store data values in key:value pairs.
# To create a dictionary, we use the curly braces({}) tis used and the key-value pairs are denoted with (key : value)

person1 = {
	"name": "Nikolai",
	"age": 18,
	"occupation": "student",
	"isEnrolled": True,
	"subjects": ["Python", "SQL", "Django"]
}

print(person1)

# To get the number of key-value pairs in the dictionary, the len() method can be used

print(len(person1))

# Accessing values in the dictionary
print(person1["name"])

# The keys() method will return a list of all keys in the dictionary
print(person1.keys())

# The values() method will return a list of all the values in the dictionary
print(person1.values())

# The items() method will return each item in a dictionary as a key-value pair
print(person1.items())

# Adding key-value pairs can be done either putting a new index key and assigning a value or the update() method

person1["nationality"] = "Filipino"
person1.update({"fave_food": "Sinigang"})
print(person1)

# Deleting entries can be done using the pop() method or the del keyword
person1.pop("fave_food")
del person1["nationality"]
print(person1)

# clear() method empties the dictionary
person2 = {
	"name": "John",
	"age": 25
}
print(person2)
person2.clear()
print(person2)

# Looping through dictionaries
for key in person1:
	print(f'The value of {key} is {person1[key]}')

# Nested Dictionaries - dictionaries can be nested inside each other
person3 = {
	"name": "Vanessa",
	"age": 22,
	"occupation": "biologist",
	"isEnrolled": True,
	"subjects": ["Python", "SQL", "Django"]
}

classRoom = {
	"student1": person1,
	"student2": person3
}

print(classRoom)

# Mini-Exercise
# 1. Create a car dictionary with the following keys
# brand, model, year of make, color
# 2. Print the following statements from the details:
# "I own a <brand> <model> and it was made in <year of make>"

car = {
	"brand": "Geely",
	"model": "Coolray",
	"year": 2021,
	"color": "navy blue"
}

print(f"I own a {car['brand']} {car['model']} and it was  made in {car['year']}")

# Functions
# Functions are blocks of code that runs when called
# The "def" keyword is used to create a function. The syntax is
# def <functionName>()

# define a function called my_greeting
def my_greeting():
	# code to be run when our my_greeting is called back
	print("Hello User")

# Calling/Invoking a function
my_greeting()

# Parameters can be added to function to have more control to what the inputs for the function will be
def greet_user(username):
	# prints out the value of the username parameter
	print(f'Hello, {username}!')
# Arguments are the values that are substituted to the parameters
greet_user("Philippe")
greet_user("Vanessa")

# From a function's perspective:
# A parameter is a variable listed inside the parenthesis in the function definition
# Argument is the value that is sent to the function when it is called

# return statement - the "return" keyword allow functions to return values
def addition(num1, num2):
	return num1+num2

sum = addition(1,2)
print(f"The sum is {sum}.")

# Lambda Functions
# A lambda function is a small anonymous function that can be used for callbacks.

greeting = lambda person: f'hello {person}'
print(greeting("Elsie"))
print(greeting("Anthony"))

multiply = lambda a, b : a * b
print(multiply(5, 6))
print(multiply(6, 99))

# Classes
# Classes would serve as blueprints to describe the concepts of objects
# To create a class, the "class" keyword is used along with the class name that starts with an uppercase character
# class ClassName()

class Car():
	# properties
	def __init__(self, brand, model, year):
		self.brand = brand
		self.model = model
		self.year = year

	# Other properties can be added and assigned hard-coded values
		self.fuel = "Gasoline" 
		self.fuel_level = 0

	#methods
	def fill_fuel(self):
		print(f'Current fuel level: {self.fuel_level}')
		print('filling up the fuel tank...')
		self.fuel_level = 100
		print(f'New fuel level: {self.fuel_level}')

	def drive(self, distance):
		print(f"The car is driven {distance} kilometers.")
		print(f"The fuel level left: {self.fuel_level - distance}")

# Creating a new instance is done by calling the class and provide the arguments

new_car = Car("Nissan", "GT-R", "2019")
# Display attributes can be done using the dot notation
print(f"My car is a {new_car.brand} {new_car.model}")

# Calling methods of the instance
new_car.fill_fuel()
new_car.drive(50)